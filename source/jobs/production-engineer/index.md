---
layout: markdown_page
title: "Production Engineer"
---

At GitLab, Production Engineers are highly independent and self-organized team
contributors in a [remote and agile](https://about.gitlab.com/2015/09/14/remote-agile-at-gitlab/) way.

A Production Engineer is a hybrid Software/System Engineer who ensures that GitLab.com
runs smoothly and have the capacity for future growth. Either you are a Developer who
loves to keep things running or a Systems Engineer who loves to code.

## Responsibilities
* As a Production engineer you will be on a pager duty rotation to respond
to GitLab.com availability incidents and customer incidents.
* Improve GitLab.com availability and performance
* Incident response for customers and for GitLab.com
* Making GitLab easier to maintain for engineers all over the world
* Manage our infrastructure with Chef (and a little Ansible)
* Improve monitoring systems
* Improve deployment processes
* Making GitLab.com scale as it grows

## Requirements for Applicants
(Check our [Jobs](https://about.gitlab.com/jobs/) page to see current openings).

* Linux expertise (we use Ubuntu Server)
* Database experience (we use PostgreSQL and Redis)
* Chef experience
* Ability to code really well in at least one language (Ruby and Ruby on Rails preferred)
* Ability to learn new languages, technologies and tools quickly
* Sharp troubleshooting skills anywhere in the stack
* Work remotely from anywhere in the world (Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/)!)

## Workflow

You work on issues in the [operations repository](https://gitlab.com/gitlab-com/operations/issues).

The priority of the issues can be found in [the handbook under GitLab Workflow](https://about.gitlab.com/handbook/#prioritize).
